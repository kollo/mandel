/******************************************************************/
/*                                                                */
/* Routine fuer die 2-D Mandelbrot-Grafikausgabe                  */
/*                                                                */
/* little program to produce Mandelbrod fractals                  */
/* output is a raw image (256 colors) default size: 256x256       */
/*                                                                */
/* to produce a gif-image:                                        */
/* mandelraw | raw2gif -p colormap -s 256 256 > my.gif            */
/*                                                                */
/* compile: gcc mandelraw.c -o mandelraw                          */
/*                                                                */
/*                                                                */
/* (c) Markus Hoffmann 1995        Letzte Bearbeitung: 30.03.1996 */
/*                                                                */
/******************************************************************/

/* This file is part of MANDEL, fractal calculation
 * ============================================================
 * VACLINE is scientific software and comes with NO WARRANTY - 
 * read the file COPYING for details
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


/*************************************/
/* GLOBALe VARIABLEn                 */
/*************************************/

#ifdef VGA800
#define DEFAULT_W 800
#define DEFAULT_H 800
#elif defined VGA640
#define DEFAULT_W 640
#define DEFAULT_H 480
#endif


#ifndef DEFAULT_W
#define DEFAULT_W 256
#endif
#ifndef DEFAULT_H
#define DEFAULT_H 256
#endif


/* Arbeitsbereich (Koordinatenbox) */

static int bx=0,by=0;
static unsigned int bw=DEFAULT_W,bh=DEFAULT_H;

static int verbose=0;    /* verbosity level */

/*  DEFAULT Werte */

static double xmin=-0.75;
static double xmax=-.722;
static double ymin=0.155;
static double ymax=0.18;


typedef struct { double re; double im; } complex; 
//static int kx(double x) {return((x-xmin)*bw/(xmax-xmin)+bx);}
//static int ky(double y) {return(-bh/(ymax-ymin)*(y-ymax)+by);}
static double ox(int x) {return((x-bx)*(xmax-xmin)/bw+xmin);}
static double oy(int y) {return(-(y-by)*(ymax-ymin)/bh+ymax);}


/* Main function */

static void doredraw() {
  int i,j,o; 
  double cz1,ii,jj;
  complex z;

  for(j=0;j<bh;j++) { 
    for(i=0;i<bw;i++) {
      z.re=z.im=0;   
      ii=ox(i); jj=oy(j);
      for(o=0;o<256;o++) {
	cz1=z.re*z.re-z.im*z.im+ii;
	z.im=2*z.re*z.im+jj;
	z.re=cz1;
	if((z.re*z.re+z.im*z.im)>16) break;
      }
      putchar(o);
    }
  }
} 
static void intro() {
  // TODO
}
static void usage() {
  fprintf(stderr,"\nUsage:\n------\n"
                 " mandelraw [options] <x1> <x2> <y1> <y2> [<bw> <bh>]\n\n"
                 "   x1  -- Koordinate [%g]\n"
                 "   x2  -- Koordinate [%g]\n"
                 "   y1  -- Koordinate [%g]\n"
                 "   y2  -- Koordinate [%g]\n"
                 "   bw  -- image width in pixels [%d]\n"
                 "   bw  -- image height in pixels [%d]\n"
                 "\n",xmin,xmax,ymin,ymax,bw,bh);
}

int main (int argc, char *argv[]) { 
  int count;

  /* Process commandline parameters */
  for(count=1;count<argc;count++) {
    if(!strcmp(argv[count],"-h") || !strcmp(argv[count],"--help")) {
      intro();
      usage();
      exit(0);   
    } 
    else if(!strcmp(argv[count],"-v")) verbose++;
    else if(!strcmp(argv[count],"-q")) verbose--;
//    else if(*argv[count]=='-') printf("Unknown option: %s\n",argv[count]);
    else {
      if(count<argc) xmin=atof(argv[count++]);
      if(count<argc) xmax=atof(argv[count++]);
      if(count<argc) ymin=atof(argv[count++]);
      if(count<argc) ymax=atof(argv[count++]);
      if(count<argc) bw=atof(argv[count++]);
      if(count<argc) bh=atof(argv[count++]);
    }
  }
  if(verbose>0) {
    printf("bitmap: [%dx%d]\n",bw,bh);
  }
  /* Do the output */
  doredraw();
  return(0);
}  /* Ende des Hauptprogramms */
