Mandel  for UNIX workstations with X
======
(c) by Markus Hoffmann 1995


/* little program to produce Mandelbrod fractals                  */
/* (c) Markus Hoffmann 1995        Letzte Bearbeitung: 30.03.1996 */


I have written this little X application as a benchmark for the UNIX
workstations we had 1995 in the University of Bonn. Back then, workstations have
been very expensive, and the speed and performance was great. However
rendering a 400x400 pixel bitmap with the mandelbrot fractal took 
several seconds. 

Back then all displays and all terminals used a monochrome or color palette
(color table). So one had either only black and white or up to 256 colors. 
Mandel has not bee properly ported to produce the same result on true color 
displays. But I have now implemented an internal color table. 

I have written this as an addition to the collection of X11 sample programs 
(xearth, xroach, ...) to have some distraction from the office work. 

I had the algorithm also running on the ATARI ST (much slower, of course) 
since the late 1980th.


The programs mandelraw, mandelraw640-480 and mandelraw800 calculate raw 
images sent to stdout. This can be used to make any sort of image files 
(e.g. .png) and use them in your scripts, web servers etc...
