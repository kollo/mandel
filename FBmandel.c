
/******************************************************************/
/*                                                                */
/* Routine fuer die 2-D Mandelbrot-Grafikausgabe                  */
/* (c) Markus Hoffmann 1995        Letzte Bearbeitung: 30.03.1996 */
/*                                                                */
/******************************************************************/

/* INCLUDEs   */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "framebuffer.h"
#define val(a)   atof(a)
extern const unsigned short colormap[];

/*********************************************************************/
/* GLOBALe VARIABLEn                                                  */
/*********************************************************************/

static double ymin,ymax,xmin,xmax;


/* Arbeitsbereich (Koordinatenbox) */
static int bx,by,bw,bh;
static int b,d;
/*  DEFAULT Werte */

static double xxmin=-0.75;
static double xxmax=-.722;
static double yymin=0.155;
static double yymax=0.18;

/* 2D-Grafikroutinen    (c) Markus Hoffmann  1993 */


//static int kx(double x) {return((x-xmin)*bw/(xmax-xmin)+bx);}
//static int ky(double y) {return(-bh/(ymax-ymin)*(y-ymax)+by);}
static double ox(int x) {return((x-bx)*(xmax-xmin)/bw+xmin);}
static double oy(int y) {return(-(y-by)*(ymax-ymin)/bh+ymax);}

static int limit=256;

 typedef struct { double re; double im; } complex; 

static void doredraw() {
  int i,j,k,l,o,x,y,w,h; double cz1,ii,jj;
  complex z;
  unsigned int r,g,b;
   
  xmin=xxmin;  ymin=yymin;  xmax=xxmax;  ymax=yymax;
  x=0;y=0;w=bw; h=bh;


  k=y+h;
  l=x+w;
  
  for(j=y;j<k;j++) { 
  for(i=x;i<l;i++) {
    
      z.re=0; z.im=0;   ii=ox(i); jj=oy(j);
      for(o=0;o<limit;o++) {
	cz1=z.re*z.re-z.im*z.im+ii;
	z.im=2*z.re*z.im+jj;
	z.re=cz1;
	if((z.re*z.re+z.im*z.im)>16) break;
      }
      o&=0xff;
      r=colormap[3*o]<<8;
      g=colormap[3*o+1]<<8;
      b=colormap[3*o+2]<<8;
      
      FB_PutPixel(i,j,FB_get_color(r,g,b));
    }
  }

}




  #define BLACK     0x0000
  #define WHITE     0xffff



int main (int argc, char *argv[]) { 

  if(argc>=5) {
    xxmin=val(argv[1]);
    xxmax=val(argv[2]);
    yymin=val(argv[3]);
    yymax=val(argv[4]);
  }
  if(argc>5) {
    limit=val(argv[5]);
  }

  FbRender_Open();
  FB_get_geometry(&bx, &by, &bw, &bh, &b, &d);
  
  Fb_BlitText(bx+10,by+bh-16,WHITE,BLACK,"mandel (c) Markus Hoffmann 1994-2008");
  doredraw();
  FbRender_Close();
  return(0);
}  /* Ende des Hauptprogramms */
