' X11-Basic program to convert colormap file to .c
' by Markus Hoffmann
' needs: X11-Basic, the BASIC interpreter
'
OPEN "I",#1,"colormap"
PRINT "unsigned short colormap[]={"
WHILE NOT EOF(#1)
  LINEINPUT #1,t$
  t$=XTRIM$(t$)
  SPLIT t$," ",0,a$,t$
  i=VAL(a$)
  SPLIT t$," ",0,a$,t$
  r=VAL(a$)
  SPLIT t$," ",0,a$,t$
  g=VAL(a$)
  SPLIT t$," ",0,a$,t$
  b=VAL(a$)
  IF NOT EOF(#1)
    PRINT "  ";r;",";g;",";b;", /*"+STR$(i)+"*/"
  ELSE
    PRINT "  ";r;",";g;",";b;"  /*"+STR$(i)+"*/"
  ENDIF
WEND
PRINT "};"
CLOSE
QUIT
