
/******************************************************************/
/*                                                                */
/* Routine fuer die 2-D Mandelbrot-Grafikausgabe                  */
/* (c) Markus Hoffmann 1995        Letzte Bearbeitung: 30.03.1996 */
/*                                                                */
/******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <math.h>
#include <sysexits.h>

#define box(x,y,w,h)    XDrawRectangle(display,win,gc,x,y,w,h)
#define pbox(x,y,w,h)   XFillRectangle(display,win,gc,x,y,w,h)
#define text2(x,y,string) XDrawImageString(display,pix,gc,x,y,string,strlen(string))
#define text(x,y,string) XDrawImageString(display,win,gc,x,y,string,strlen(string))
#define color(x)        XSetForeground(display,gc,x)

extern const unsigned short colormap[];

static char *strs( double a, int n, int f, char *t){
  char format[20]="%e"; 
  static char output[30];
  
  if(n==0 && f==0) strcpy(format,"%g");
  if(f!=0)         strcpy(format,"%g");

  sprintf(output,format,a);
  strcpy(t,output);
  return(output);
}


/*************************/
/* GLOBALe VARIABLEn     */
/*************************/

double ymin,ymax,xmin,xmax;
int dummy,s,flag;
int verbose=1;

/* Screen-koordinaten   */
int sbx,sby;
unsigned int sbw,sbh;


int bx,by,bw,bh;  /* Arbeitsbereich (Koordinatenbox) */

//static int kx(double x) {return((x-xmin)*bw/(xmax-xmin)+bx);}
//static int ky(double y) {return(-bh/(ymax-ymin)*(y-ymax)+by);}
static double ox(int x) {return((x-bx)*(xmax-xmin)/bw+xmin);}
static double oy(int y) {return(-(y-by)*(ymax-ymin)/bh+ymax);}

/*  DEFAULT Werte */

double xxmin=-0.75;
double xxmax=-.722;
double yymin=0.155;
double yymax=0.18;

/* Window-Verwaltung ... */

Display *display;            /* Verbindung zu X-Server */
Window win;                 /* Das wird das Fenster */
Pixmap pix;
unsigned int x = 0, y = 0, w = 256, h = 256;
unsigned long border = 4, background, foreground;
char *wname = "Markus Hoffmann Grafikoutput";
char *iname = "Mandelbrot-Plot";
XSizeHints size_hints;       /* Hinweise fuer den WIndow-Manager..*/
XWMHints wm_hints;
XClassHint class_hint;
XTextProperty win_name, icon_name;
GC gc;                      /* Im Gc wird Font, Farbe, Linienart, u.s.w. festgelegt. */
XGCValues gc_val;
static char dash_list [] = { 35, 1};


 typedef struct { double re; double im; } complex; 

static void doredraw(int x, int y, int w, int h, int f) {
  int i,j,k,l,o; double cz1,ii,jj;
  complex z;
  char uuu[30];
  Window root;
  unsigned int b;
  unsigned int d;
  
  XGetGeometry(display,win,&root,&sbx,&sby,&sbw,&sbh,&b,&d); 

  if(bw!=sbw || bh != sbh || f) {
    XFreePixmap(display,pix);
    bx=sbx;  by=sby;  bw=sbw;  bh=sbh;
    xmin=xxmin;  ymin=yymin;  xmax=xxmax;  ymax=yymax;
    w=bw; h=bh;
    pix=XCreatePixmap(display,win,w,h,d);

  /* Hier wird gezeichnet .... ==================== */ 
  color(0x00ff00);
  text(bx+10,by+10,"... busy ...");

  k=y+h;
  l=x+w;
  for(i=x;i<l;i++) {
    for(j=y;j<k;j++) { 
      z.re=0; z.im=0;   ii=ox(i); jj=oy(j);
      for(o=0;o<256;o++) {
	cz1=z.re*z.re-z.im*z.im+ii;
	z.im=2*z.re*z.im+jj;
	z.re=cz1;
	if((z.re*z.re+z.im*z.im)>16) break;
      }

/* Here we make assumptions about the color depth etc. Originally back in 1990th
 there were only displays with color map, now we have true color of some depth...*/
      color((colormap[o*3]<<16) | (colormap[o*3+1]<<8) | colormap[o*3+2]);

   //   color(o); /* use this if a color map is used on the display */
      XDrawPoint(display,pix,gc,i,j);
    }
  }
  color(0xFF0000);  /* should be red*/
  text2(bx+10,by+10,"Coordinates:");
  strs(xmin,0,0,uuu);  text2(bx+10,by+25,uuu); 
  strs(xmax,0,0,uuu);  text2(bx+90,by+25,uuu);
  strs(ymin,0,0,uuu);  text2(bx+160,by+25,uuu); 
  strs(ymax,0,0,uuu);  text2(bx+240,by+25,uuu);
    if(verbose) {
      printf("Coordinates: x=(%g:%g) y=(%g:%g)\n",xmin,xmax,ymin,ymax);
      printf("Screen: bx=%d by=%d bw=%d bh=%d\n",bx,by,bw,bh);
    
    }
  } 
  XCopyArea(display, pix,win,gc,x,y,w,h,x,y);
}

int main (int argc, char *argv[]) { 
  char *display_name = NULL;   /* NULL: Nimm Argument aus setenv DISPLAY */
  int screen_num;              /* Ein Server kann mehrere Bildschirme haben */
  int nx=0,ny=0,nw,nh; 
  double dnx,dny;
  int count=1;

  /* take exactly 4 commandline parameters */

  if(argc>=5) {
    xxmin=atof(argv[count++]);
    xxmax=atof(argv[count++]);
    yymin=atof(argv[count++]);
    yymax=atof(argv[count++]);
  }

   
  /* Verbindung zum X-Server aufnehmen. */
  if((display=XOpenDisplay(display_name)) == NULL) {
    fprintf(stderr, "Can't Connect XServer on display %s. Aborted\n",XDisplayName(display_name));
    exit(EX_UNAVAILABLE);
  }
  /* Welchen Bildschirm nehmen ? */
  screen_num = DefaultScreen(display);
  if(verbose) printf("Screen number: %d\n",screen_num);


  /* Fenster Oeffnen */
  background = BlackPixel(display, screen_num);
  foreground = WhitePixel(display, screen_num);
  if(verbose) printf("Colors: b=0x%lx f=0x%lx\n",background,foreground);
  
  win = XCreateSimpleWindow(display, RootWindow(display, screen_num),
			    x, y, w, h, border, foreground, background);
  pix=XCreatePixmap(display,win,w,h,8);
  /* Dem Window-Manager Hinweise geben, was er mit der Groesse des Fensters 
     machen soll. */
  size_hints.flags = PPosition | PSize | PMinSize;
  size_hints.min_width = 100;
  size_hints.min_height = 100;
  wm_hints.flags = StateHint | InputHint;
  wm_hints.initial_state = NormalState;
  wm_hints.input = True;
  class_hint.res_name = "Mandelb";
  class_hint.res_class = "Output";
  if (!XStringListToTextProperty(&wname, 1, &win_name) ||
      !XStringListToTextProperty(&iname, 1, &icon_name)) {
    fprintf(stderr, "Couldn't set Name of Window or Icon. Aborted\n");
    exit(EX_OSERR);
  }

 /* Man XSetWMProperties, um zu lesen, was passiert ! */
  XSetWMProperties(display, win, &win_name, &icon_name, argv, argc, 
		   &size_hints, &wm_hints, &class_hint);

  /* Auswaehlen, welche Events man von dem Fenster bekommen moechte */
  XSelectInput(display, win, 
	       ExposureMask | ButtonPressMask);
  
  /* Graphic-Context anlegen */
  gc = XCreateGC(display, win, 0, &gc_val);
  XSetForeground(display, gc, foreground);
  XSetLineAttributes(display, gc, 1, LineOnOffDash, CapRound, JoinRound);
  XSetDashes(display, gc, 0, dash_list, 2);

  /* Das Fensterauf den Screen Mappen */
  XMapWindow(display, win);

  /* !!!!!!!
 Das Fenster wird erst dann gemalt, wenn man Events davon abfragt !!!!!!!! */

  /* Die Event-Schleife */

  for(;;) {
    XEvent event; char uuu[20];
    
    XNextEvent(display, &event);
    switch (event.type) {

    /* Das Redraw-Event */  
    case Expose:
      /* if (event.xexpose.count != 0)    break; */
     
    
      doredraw(event.xexpose.x,event.xexpose.y,event.xexpose.width,event.xexpose.height,0);
      break;
    
    /* Bei Mouse-Taste: Ende */
    case ButtonPress: 
      strs(event.xbutton.x,0,0,uuu);  text(bx,bh,uuu); 
      strs(event.xbutton.y,0,0,uuu);  text(bx+50,bh,uuu);
      strs(event.xbutton.button,0,0,uuu);  text(bx,100,uuu); 
      strs(event.xbutton.y,0,0,uuu);  text(bx+50,bh,uuu);
      if(event.xbutton.button==1) {
	if (flag==1) {
          xxmin=ox(nx); yymin=oy(ny);
          xxmax=ox(event.xbutton.x);
          yymax=oy(event.xbutton.y);
	  nw=event.xbutton.x-nx;nh=event.xbutton.y-ny;
          if(xxmax<xxmin) {dnx=xxmax; xxmax=xxmin; xxmin=dnx;} 
          if(yymax<yymin) {dnx=yymax; yymax=yymin; yymin=dnx;}
	  color(0);
          pbox(bx+bw/4,by+bh/4,bw/2,bh/2);
          color(1);
          box(bx+bw/4,by+bh/4,bw/2,bh/2);
          XCopyArea(display,pix,win,gc,nx,ny,nw,nh,bx+bw/2-nw/2,by+bh/2-nh/2);
          XCheckWindowEvent(display,win,ExposureMask, &event);
          doredraw(bx,by,bw,bh,1);
          flag=0;
	} else {
          nx=event.xbutton.x;
          ny=event.xbutton.y;
          box(nx,ny,5,5);
          flag=1;
	}
      } else if(event.xbutton.button==2) {
          dnx=xxmin-(xxmax-xxmin);
          dny=yymin-(yymax-yymin);
          xxmax=xxmax+(xxmax-xxmin);
          yymax=yymax+(yymax-yymin);
          xxmin=dnx; yymin=dny;
          if(xxmax<xxmin) {dnx=xxmax; xxmax=xxmin; xxmin=dnx;} 
          if(yymax<yymin) {dnx=yymax; yymax=yymin; yymin=dnx;}
          color(0);
          pbox(bx+bw/4,by+bh/4,bw/2,bh/2);
          color(1);
          box(bx+bw/4,by+bh/4,bw/2,bh/2);
          doredraw(bx,by,bw,bh,1);
          flag=0;
      } else if(event.xbutton.button==3) {
        /* Alles aufraeumen und beenden */
        XFreeGC(display, gc);
        XCloseDisplay(display);
        exit(EX_OK);
      }
      break;
    }
  }
  return(EX_OK);
}  /* Ende des Hauptprogramms */
