Guide to contributing to MANDEL
===============================

MANDEL is a collection of programs which produce images of the mandelbrot 
fractal. The main application is a X11/UNIX program for UNIX workstations
which also comiled on LINUX. It has been along since 1995. 

The functionallyty of mandel is limited, but the program and also
the whole package can of course be improved.

Suggestions for improvements are therefor very welcome. Even the exchange 
of experience of usage could be very valuable.


You can:
* report bugs and unexpected behaviour. --> open issue
* donate/share your magnet or geometry files for testing.
* discuss new features  --> open issue
* improve the user manual and online-help.


## License and attribution

All contributions must be properly licensed and attributed. If you are
contributing your own original work, then you are offering it under a CC-BY
license (Creative Commons Attribution). If it is code, you are offering it under
the GPL-v2. You are responsible for adding your own name or pseudonym in the
Acknowledgments file, as attribution for your contribution.

If you are sourcing a contribution from somewhere else, it must carry a
compatible license. The project was initially released under the GNU public
licence GPL-v2 which means that contributions must be licensed under open
licenses such as MIT, CC0, CC-BY, etc. You need to indicate the original source
and original license, by including a comment above your contribution. 


## Contributing with a Merge Request

The best way to contribute to this project is by making a merge request:

1. Login with your Gitlab account or create one now
2. [Fork](https://gitlab.com/kollo/mamdel.git) the mandel repository.  Work on your fork.
3. Create a new branch on which to make your change, e.g.
`git checkout -b my_code_contribution`, or make the change on the `new` branch.
4. Edit the file where you want to make a change or create a new file in the  `contrib` directory if you're not sure where your contribution might fit.
5. Edit `ACKNOWLEGEMENTS` and add your own name to the list of contributors under the section with the current year. Use your name, or a gitlab ID, or a pseudonym.
6. Commit your change. Include a commit message describing the correction.
7. Submit a merge request against the mandel repository.



## Contributing with an Issue

If you find a mistake and you're not sure how to fix it, or you don't know how
to do a merge request, then you can file an Issue. Filing an Issue will help us
see the problem and fix it.

Create a [new Issue](https://gitlab.com/kollo/mandel/issues/new?issue) now!


## Thanks

We are very grateful for your support. With your help, this implementation
will be a great project. 


So you are welcome to help.
