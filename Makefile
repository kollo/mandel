# Makefile for the mandel collection by Markus Hoffmann 1996-2016
LIBNO=1.01
NAME=mandel

# Directories
prefix=/usr
exec_prefix=${prefix}
BINDIR=${exec_prefix}/bin
MANDIR=${prefix}/share/man
DATADIR=${prefix}/share

CFLAGS =  -O3 -Wall
LINKFLAGS = -lm -lX11

ARMCC=arm-linux-gcc

HSRC=framebuffer.h
CSRC=colormap.c FBmandel.c framebuffer.c mandel.c mandelraw.c

BINDIST= README.md mandel mandelraw mandelraw800 mandelraw640-480 

DIST= $(HSRC) $(CSRC)  README.md COPYING Makefile colormap col.bas


all : mandel mandelraw mandelraw800 FBmandel

mandel.o : mandel.c Makefile
	gcc -c $(CFLAGS) mandel.c
colormap.o : colormap.c Makefile
	gcc -c $(CFLAGS) colormap.c

mandel : mandel.o  colormap.o
	gcc $(CFLAGS) -o $@ mandel.o colormap.o $(LINKFLAGS)
	strip $@

# make C sources out of a binary colormap file
colormap.c: colormap col.bas
	xbasic col.bas > $@

FBmandel : FBmandel.o framebuffer.o colormap.c
	gcc $(CFLAGS) -o FBmandel FBmandel.o framebuffer.o colormap.c
	strip $@
TTmandel : FBmandel.c colormap.c framebuffer.c
	$(ARMCC) -O3 -o $@  FBmandel.c framebuffer.c colormap.c

mandelraw: mandelraw.c
	gcc -O3 -Wall -o $@ $<
	strip $@
mandelraw800: mandelraw.c
	gcc -O3 -DVGA800 -Wall -o $@ $<
	strip $@
mandelraw640-480: mandelraw.c
	gcc -O3 -DVGA640 -Wall -o $@ $<
	strip $@

install : $(NAME) 
	install -m 755  $(NAME) $(BINDIR)/
uninstall :
	rm -f $(BINDIR)/$(NAME) 
doc-pak: README.md
	mkdir -p $@
	cp $+ $@/
dist :	$(NAME)-$(LIBNO).tar.gz
$(NAME)-$(LIBNO).tar.gz : $(DIST)
	rm -rf /tmp/$(NAME)-$(LIBNO)
	mkdir /tmp/$(NAME)-$(LIBNO)
	(tar cf - $(DIST))|(cd /tmp/$(NAME)-$(LIBNO); tar xpf -)
	(cd /tmp; tar cf - $(NAME)-$(LIBNO)|gzip -9 > $@)
	mv /tmp/$@ .
deb :	$(BINDIST) doc-pak
	sudo checkinstall -D --pkgname $(NAME) --pkgversion $(LIBNO) --arch i386 \
	--maintainer kollo@users.sourceforge.net  \
	--requires libc6 --backup \
	--pkggroup Science --provides $(NAME)

clean :
	rm -f *.o backup-*.tgz 
distclean: clean
	rm -f mandel mandel.exe mandelraw mandelraw800 FBmandel TTmandel
	rm -rf doc-pak
